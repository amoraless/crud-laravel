USE [db_saas_cif_2]
GO
/****** Object:  StoredProcedure [saas].[usp_categoria_delete]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_categoria_delete]
@db_mensaje					varchar(1000) output,
@id_categoria				int
as
	set nocount on
	declare @categoria varchar(100) = (select categoria from saas.categoria where id_categoria = @id_categoria)
	declare @estado_actual varchar(4)
	begin try		
		update	saas.categoria 
		set		categoria_estado = (case when categoria_estado = '1' then '0' else '1' end),
				@estado_actual = (case when categoria_estado = '1' then 'baja' else 'alta' end),
				categoria_fm = GETDATE()
		where	saas.categoria.id_categoria= @id_categoria
		set @db_mensaje = '1-Se ha dado de '+cast(@estado_actual as varchar)+' en la base de datos los datos de: (Id:'+cast(@id_categoria as varchar)+') <strong>'+@categoria+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje = '0-No se ha podido completar la operación.  
						   Contactarse con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_categoria_insert]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [saas].[usp_categoria_insert]
@db_mensaje					varchar(1000) output,
@categoria					varchar(100),
@categoria_codigo			varchar(12),
@categoria_descripcion		text,
@categoria_estado			char(1)
as
	set nocount on
	begin try
		insert into saas.categoria(categoria,categoria_codigo ,categoria_descripcion, categoria_fr, categoria_fm, categoria_estado)
		values(@categoria,@categoria_codigo ,@categoria_descripcion, GETDATE(), GETDATE(),@categoria_estado)
		set @db_mensaje = '1-Se ha guardado en la base de datos los datos de: (Id:'+cast(@@IDENTITY as varchar)+') <strong>'+@categoria+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())
		set @db_mensaje =	'0-No se ha podido guardar en la base de datos los datos de: <strong>'+@categoria+'</strong>. 
							 Verifique que el nombre de la Categoría no esté ya registrada puesto que no se puede aceptar dos categorias con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	return

GO
/****** Object:  StoredProcedure [saas].[usp_categoria_list]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_categoria_list]
@dato	varchar(100)
as
	set nocount on
	select	id_categoria, categoria,categoria_codigo, categoria_descripcion, categoria_fr, categoria_fm, categoria_estado
	from	saas.categoria
	where	saas.categoria.categoria LIKE('%'+@dato+'%')

GO
/****** Object:  StoredProcedure [saas].[usp_categoria_select]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_categoria_select]
@id_categoria	int
as
	set nocount on
	select	id_categoria, categoria, categoria_codigo,categoria_descripcion, categoria_fr, categoria_fm,categoria_estado
	from	saas.categoria
	where	saas.categoria.id_categoria = @id_categoria

GO
/****** Object:  StoredProcedure [saas].[usp_categoria_update]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_categoria_update]
@db_mensaje					varchar(1000) output,
@id_categoria				int,
@categoria					varchar(100),
@categoria_codigo			varchar(12),
@categoria_descripcion		text,
@categoria_estado			char(1)
as
	set nocount on
	begin try		
		update	saas.categoria 
		set		categoria = @categoria,
				categoria_codigo=@categoria_codigo,
				categoria_descripcion = @categoria_descripcion,
				categoria_estado = @categoria_estado,
				categoria_fm = GETDATE()
		where	saas.categoria.id_categoria= @id_categoria
		set @db_mensaje = '1-Se ha actualizado en la base de datos los datos de: (Id:'+cast(@id_categoria as varchar)+') <strong>'+@categoria+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje =	'0-No se ha podido actualizar en la base de datos los datos de: <strong>'+@categoria+'</strong>. 
							 Verifique que el nombre de la Categoría no esté ya registrada puesto que no se puede aceptar dos Categorias con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_gerencia_delete]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_gerencia_delete]
@db_mensaje					varchar(1000) output,
@id_gerencia				int
as
	set nocount on
	declare @gerencia varchar(100) = (select gerencia from saas.gerencia where id_gerencia = @id_gerencia)
	begin try		
		update	saas.gerencia 
		set		gerencia_estado = (case when gerencia_estado = '1' then '0' else '1' end),
				gerencia_fm = GETDATE()
		where	saas.gerencia.id_gerencia = @id_gerencia
		set @db_mensaje = '1-Se ha dado de baja en la base de datos los datos de: (Id:'+cast(@id_gerencia as varchar)+') <strong>'+@gerencia+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje = '0-No se ha podido dar de baja en la base de datos los datos de: <strong>'+@gerencia+'</strong>. 
						   Contactarse con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_gerencia_insert]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--PROCEDIMIENTOS PARA GERENCIA INICIO
create procedure [saas].[usp_gerencia_insert]
@db_mensaje					varchar(1000) output,
@gerencia					varchar(100),
@gerencia_descripcion		text,
@gerencia_estado			char(1)
as
	set nocount on
	begin try
		insert into saas.gerencia(gerencia, gerencia_descripcion, gerencia_fr, gerencia_fm, gerencia_estado)
		values(@gerencia, @gerencia_descripcion, GETDATE(), GETDATE(),@gerencia_estado)
		set @db_mensaje = '1-Se ha guardado en la base de datos los datos de: (Id:'+cast(@@IDENTITY as varchar)+') <strong>'+@gerencia+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())
		set @db_mensaje =	'0-No se ha podido guardar en la base de datos los datos de: <strong>'+@gerencia+'</strong>. 
							 Verifique que el nombre de la gerencia no esté ya registrada puesto que no se puede aceptar dos gerencias con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	return

GO
/****** Object:  StoredProcedure [saas].[usp_gerencia_list]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_gerencia_list]
@dato	varchar(100)
as
	set nocount on
	select	id_gerencia, gerencia, gerencia_descripcion, gerencia_fr, gerencia_fm, gerencia_estado
	from	saas.gerencia
	where	saas.gerencia.gerencia LIKE('%'+@dato+'%')

GO
/****** Object:  StoredProcedure [saas].[usp_gerencia_select]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_gerencia_select]
@id_gerencia	int
as
	set nocount on
	select	id_gerencia, gerencia, gerencia_descripcion, gerencia_fr, gerencia_fm, gerencia_estado
	from	saas.gerencia
	where	saas.gerencia.id_gerencia = @id_gerencia

GO
/****** Object:  StoredProcedure [saas].[usp_gerencia_update]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_gerencia_update]
@db_mensaje					varchar(1000) output,
@id_gerencia				int,
@gerencia					varchar(100),
@gerencia_descripcion		text,
@gerencia_estado			char(1)
as
	set nocount on
	begin try		
		update	saas.gerencia 
		set		gerencia = @gerencia,
				gerencia_descripcion = @gerencia_descripcion,
				gerencia_estado = @gerencia_estado,
				gerencia_fm = GETDATE()
		where	saas.gerencia.id_gerencia = @id_gerencia
		set @db_mensaje = '1-Se ha actualizado en la base de datos los datos de: (Id:'+cast(@id_gerencia as varchar)+') <strong>'+@gerencia+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje =	'0-No se ha podido actualizar en la base de datos los datos de: <strong>'+@gerencia+'</strong>. 
							 Verifique que el nombre de la gerencia no esté ya registrada puesto que no se puede aceptar dos gerencias con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_marca_delete]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_marca_delete]
@db_mensaje				varchar(1000) output,
@id_marca				int
as
	set nocount on
	declare @marca varchar(100) = (select marca from saas.marca where id_marca = @id_marca)
	declare @estado_actual varchar(4)
	begin try		
		update	saas.marca 
		set		marca_estado = (case when marca_estado = '1' then '0' else '1' end),
				@estado_actual = (case when marca_estado = '1' then 'baja' else 'alta' end),
				marca_fm = GETDATE()
		where	saas.marca.id_marca = @id_marca
		
		set @db_mensaje = '1-Se ha dado de '+cast(@estado_actual as varchar)+' en la base de datos los datos de: (Id:'+cast(@id_marca as varchar)+') <strong>'+@marca+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje = '0-No se ha podido completar la operación. 
						   Contactarse con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_marca_insert]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--PROCEDIMIENTOS PARA marca INICIO
create procedure [saas].[usp_marca_insert]
@db_mensaje			varchar(1000) output,
@marca				varchar(20),
@marca_estado		char(1)
as
	set nocount on
	begin try
		insert into saas.marca(marca, marca_fr, marca_fm, marca_estado)
		values(@marca, GETDATE(), GETDATE(),@marca_estado)
		set @db_mensaje = '1-Se ha guardado en la base de datos los datos de: (Id:'+cast(@@IDENTITY as varchar)+') <strong>'+@marca+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())
		set @db_mensaje =	'0-No se ha podido guardar en la base de datos los datos de: <strong>'+@marca+'</strong>. 
							 Verifique que el nombre de la marca no esté ya registrado puesto que no se puede aceptar dos marcas con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	return

GO
/****** Object:  StoredProcedure [saas].[usp_marca_list]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_marca_list]
@dato	varchar(100)
as
	set nocount on
	select	id_marca, marca, marca_fr, marca_fm, marca_estado
	from	saas.marca
	where	saas.marca.marca LIKE('%'+@dato+'%')

GO
/****** Object:  StoredProcedure [saas].[usp_marca_select]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_marca_select]
@id_marca	int
as
	set nocount on
	select	id_marca,marca,marca_fr, marca_fm, marca_estado
	from	saas.marca
	where	saas.marca.id_marca = @id_marca

GO
/****** Object:  StoredProcedure [saas].[usp_marca_update]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_marca_update]
@db_mensaje			varchar(1000) output,
@id_marca			int,
@marca				varchar(25),
@marca_estado		char(1)
as
	set nocount on
	begin try		
		update	saas.marca 
		set		marca = @marca,
				marca_estado = @marca_estado,
				marca_fm = GETDATE()
		where	saas.marca.id_marca = @id_marca
		set @db_mensaje = '1-Se ha actualizado en la base de datos los datos de: (Id:'+cast(@id_marca as varchar)+') <strong>'+@marca+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje =	'0-No se ha podido actualizar en la base de datos los datos de: <strong>'+@marca+'</strong>. 
							 Verifique que el nombre marca no esté ya registrado puesto que no se puede aceptar dos marcas con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_sub_categoria_delete]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_sub_categoria_delete]
@db_mensaje					varchar(1000) output,
@id_sub_categoria				int
as
	set nocount on
	declare @sub_categoria varchar(100) = (select sub_categoria from saas.sub_categoria where id_sub_categoria = @id_sub_categoria)
	declare @estado_actual varchar(4)
	begin try		
		update	saas.sub_categoria 
		set		sub_categoria_estado = (case when sub_categoria_estado = '1' then '0' else '1' end),
				@estado_actual = (case when sub_categoria_estado = '1' then 'baja' else 'alta' end),
				sub_categoria_fm = GETDATE()
		where	saas.sub_categoria.id_sub_categoria= @id_sub_categoria
		set @db_mensaje = '1-Se ha dado de '+cast(@estado_actual as varchar)+' en la base de datos los datos de: (Id:'+cast(@id_sub_categoria as varchar)+') <strong>'+@sub_categoria+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje = '0-No se ha podido completar la operación.  
						   Contactarse con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_sub_categoria_insert]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [saas].[usp_sub_categoria_insert]
@db_mensaje					varchar(1000) output,
@id_categoria					int,
@sub_categoria					varchar(100),
@sub_categoria_codigo			varchar(12),
@sub_categoria_descripcion		text,
@sub_categoria_estado			char(1)
as
	set nocount on
	begin try
		insert into saas.sub_categoria(id_categoria,sub_categoria,sub_categoria_codigo ,sub_categoria_descripcion, sub_categoria_fr, sub_categoria_fm, sub_categoria_estado)
		values(@id_categoria,@sub_categoria,@sub_categoria_codigo ,@sub_categoria_descripcion, GETDATE(), GETDATE(),@sub_categoria_estado)
		set @db_mensaje = '1-Se ha guardado en la base de datos los datos de: (Id:'+cast(@@IDENTITY as varchar)+') <strong>'+@sub_categoria+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())
		set @db_mensaje =	'0-No se ha podido guardar en la base de datos los datos de: <strong>'+@sub_categoria+'</strong>. 
							 Verifique que el nombre de la Sub Categoría no esté ya registrada puesto que no se puede aceptar dos Sub categorias con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	return

GO
/****** Object:  StoredProcedure [saas].[usp_sub_categoria_list]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_sub_categoria_list]
@dato	varchar(100)
as
	set nocount on
	SELECT     id_sub_categoria,sub_categoria,sub_categoria_codigo,sub_categoria_descripcion, 
               sub_categoria_fr,sub_categoria_fm,sub_categoria_estado,categoria.id_categoria, 
                categoria,categoria_codigo
FROM         sub_categoria INNER JOIN
                      categoria ON sub_categoria.id_categoria = categoria.id_categoria
	where	sub_categoria.sub_categoria LIKE('%'+@dato+'%')

GO
/****** Object:  StoredProcedure [saas].[usp_sub_categoria_select]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_sub_categoria_select]
@id_sub_categoria	int
as
	set nocount on
	select	id_sub_categoria,id_categoria, sub_categoria, sub_categoria_codigo,sub_categoria_descripcion, sub_categoria_fr, sub_categoria_fm,sub_categoria_estado
	from	saas.sub_categoria
	where	saas.sub_categoria.id_sub_categoria = @id_sub_categoria

GO
/****** Object:  StoredProcedure [saas].[usp_sub_categoria_update]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

CREATE procedure [saas].[usp_sub_categoria_update]
@db_mensaje					varchar(1000) output,
@id_sub_categoria				int,
@id_categoria				int,
@sub_categoria					varchar(100),
@sub_categoria_codigo			varchar(12),
@sub_categoria_descripcion		text,
@sub_categoria_estado			char(1)
as
	set nocount on
	begin try		
		update	saas.sub_categoria 
		set		id_categoria=@id_categoria,
				sub_categoria = @sub_categoria,
				sub_categoria_codigo=@sub_categoria_codigo,
				sub_categoria_descripcion = @sub_categoria_descripcion,
				sub_categoria_estado = @sub_categoria_estado,
				sub_categoria_fm = GETDATE()
		where	saas.sub_categoria.id_sub_categoria= @id_sub_categoria
		set @db_mensaje = '1-Se ha actualizado en la base de datos los datos de: (Id:'+cast(@id_sub_categoria as varchar)+') <strong>'+@sub_categoria+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje =	'0-No se ha podido actualizar en la base de datos los datos de: <strong>'+@sub_categoria+'</strong>. 
							 Verifique que el nombre de la Categoría no esté ya registrada puesto que no se puede aceptar dos Categorias con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_unidad_medida_delete]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_unidad_medida_delete]
@db_mensaje					varchar(1000) output,
@id_unidad_medida		int
as
	set nocount on
	declare @unidad_medida varchar(100) = (select unidad_medida from saas.unidad_medida where id_unidad_medida = @id_unidad_medida)
	declare @estado_actual varchar(4)
	begin try		
		update	saas.unidad_medida 
		set		unidad_medida_estado = (case when unidad_medida_estado = '1' then '0' else '1' end),
				@estado_actual = (case when unidad_medida_estado = '1' then 'baja' else 'alta' end),
				unidad_medida_fm = GETDATE()
		where	saas.unidad_medida.id_unidad_medida = @id_unidad_medida
		
		set @db_mensaje = '1-Se ha dado de '+cast(@estado_actual as varchar)+' en la base de datos los datos de: (Id:'+cast(@id_unidad_medida as varchar)+') <strong>'+@unidad_medida+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje = '0-No se ha podido completar la operación. 
						   Contactarse con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
/****** Object:  StoredProcedure [saas].[usp_unidad_medida_insert]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--PROCEDIMIENTOS PARA unidad_medida INICIO
create procedure [saas].[usp_unidad_medida_insert]
@db_mensaje						varchar(1000) output,
@unidad_medida					varchar(15),
@unidad_medida_sigla			varchar(4),
@unidad_medida_descripcion		text,
@unidad_medida_estado			char(1)
as
	set nocount on
	begin try
		insert into saas.unidad_medida(unidad_medida, unidad_medida_sigla, unidad_medida_descripcion, unidad_medida_fr, unidad_medida_fm, unidad_medida_estado)
		values(@unidad_medida, @unidad_medida_sigla, @unidad_medida_descripcion, GETDATE(), GETDATE(),@unidad_medida_estado)
		set @db_mensaje = '1-Se ha guardado en la base de datos los datos de: (Id:'+cast(@@IDENTITY as varchar)+') <strong>'+@unidad_medida+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())
		set @db_mensaje =	'0-No se ha podido guardar en la base de datos los datos de: <strong>'+@unidad_medida+'</strong>. 
							 Verifique que el nombre de la unidad de medidad no esté ya registrado puesto que no se puede aceptar dos unidades de medidad con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	return

GO
/****** Object:  StoredProcedure [saas].[usp_unidad_medida_list]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_unidad_medida_list]
@dato	varchar(100)
as
	set nocount on
	select	id_unidad_medida, unidad_medida, unidad_medida_sigla, unidad_medida_descripcion, unidad_medida_fr, unidad_medida_fm, unidad_medida_estado
	from	saas.unidad_medida
	where	saas.unidad_medida.unidad_medida LIKE('%'+@dato+'%')

GO
/****** Object:  StoredProcedure [saas].[usp_unidad_medida_select]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_unidad_medida_select]
@id_unidad_medida	int
as
	set nocount on
	select	id_unidad_medida, unidad_medida, unidad_medida_sigla, unidad_medida_descripcion, unidad_medida_fr, unidad_medida_fm, unidad_medida_estado
	from	saas.unidad_medida
	where	saas.unidad_medida.id_unidad_medida = @id_unidad_medida

GO
/****** Object:  StoredProcedure [saas].[usp_unidad_medida_update]    Script Date: 03/01/2015 22:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------------------------------------------

create procedure [saas].[usp_unidad_medida_update]
@db_mensaje						varchar(1000) output,
@id_unidad_medida				int,
@unidad_medida					varchar(15),
@unidad_medida_sigla			varchar(4),
@unidad_medida_descripcion		text,
@unidad_medida_estado			char(1)
as
	set nocount on
	begin try		
		update	saas.unidad_medida 
		set		unidad_medida = @unidad_medida,
				unidad_medida_sigla = @unidad_medida_sigla,
				unidad_medida_descripcion = @unidad_medida_descripcion,
				unidad_medida_estado = @unidad_medida_estado,
				unidad_medida_fm = GETDATE()
		where	saas.unidad_medida.id_unidad_medida = @id_unidad_medida
		set @db_mensaje = '1-Se ha actualizado en la base de datos los datos de: (Id:'+cast(@id_unidad_medida as varchar)+') <strong>'+@unidad_medida+'</strong>'
	end try
	begin catch
		insert into saas.log_errores(error_numero, error_procedimiento, error_linea, error_severidad, error_mensaje, error_fecha, error_estado)
		values (ERROR_NUMBER(), ERROR_PROCEDURE(), ERROR_LINE(), ERROR_SEVERITY(), ERROR_MESSAGE(), GETDATE(), ERROR_STATE())

		set @db_mensaje =	'0-No se ha podido actualizar en la base de datos los datos de: <strong>'+@unidad_medida+'</strong>. 
							 Verifique que el nombre del unidad de medida no esté ya registrado puesto que no se puede aceptar dos unidades de medida con el mismo nombre. Contactarse 
							 con el Área de TI si el problema persiste. Código de error referente al problema: ( <strong>Nro. ' + cast(@@IDENTITY as varchar)+' </strong>)'
	end catch
	
return

GO
